package com.appmanager.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class AppManagerDiscoveryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppManagerDiscoveryServiceApplication.class, args);
	}

}
